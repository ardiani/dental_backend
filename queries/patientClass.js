const db_conf = require('../config/db');

class patientClass {
    
    //Returns all the patients that are on the clinics record. 
    async getAllPatients(){
        let query = `SELECT * FROM PATIENT`;
        db_conf.query(query,(err,res)=>{
            if(err) return null;
            else await(res);
        });
    }

    //Registers a new patient with object values from user input
    async registerNewPatient(patientDetails){
        let _p = patientDetails;
        let insert_query = `INSERT INTO patient (NAME, SURNAME,MIDDLE_NAME,AGE,BIRTHDATE,PHONE,EMAIL,ADDRESS,CITY,GENDER,LAST_VISIT)
                                VALUES('${_p.first_name}','${_p.last_name}','${_p.middle_name}',${_p.age},${_p.birthdate},
                                        '${_p.phone}','${_p.email}','${_p.address}','${_p.city}','${_p.gender}',${_p.last_visit});`
        db_conf.query(insert_query,(err,res)=>{
            if(err) return null; // ##TODO## :  INSERT INTO ERROR_LOGS;
            else await(res);
        });
    }

    async removePatient(patientId){
        let delete_query = `DELETE FROM PATIENT WHERE ID = ${patientId}`;
        db_conf.query(delete_query,(err,res)=>{
            if(err) return null; // ##TODO## : INSERT INTO ERROR_LOGS; AND RETURN NOT FOUND!
            else await(res);
        });

    }

    async editPatient(patientId){

    }
}

module.exports = patientClass;