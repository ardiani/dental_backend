const express = require('express');
const app = express();
const port = 5000;
const cors = require('cors');
const patients = require('./routes/patient'); 

app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());

//CONFIGURING SERVER ACCESSIBILITY BY OTHER SYSTEMS: 
app.use((res,req,next)=>{
    res.header('Access-Control-Allow-Origin','*');
    res.header('Access-Control-Allow-Header','*');
    next();
});

//CONFIGURING ENDPOINT ENTRIES :
app.use('/patient',patients);  //Entry point for all patient activity. 

app.listen(port,()=>{
    console.log(`Server running on port: ${port}.`);
});
