const express =require('express');
const router = express.Router();
const patientClass = require('../queries/patientClass');
const patient = new patientClass(); 


router.post('/register',(req,res)=>{
    patient.registerNewPatient(req.body).then(result=>{
        res.send(result);
    });
});

router.get('/',(req,res)=>{
    patient.getAllPatients().then(result=>{
        res.send(result);
    });
});

module.exports = router;